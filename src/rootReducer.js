import {combineReducers} from 'redux';
import {USER_SIGNED_OUT} from './actions/types';

import patients from './reducers/patients';
import access from './reducers/access';
import pains from './reducers/pains';
import activities from './reducers/activities';
import streams from './reducers/streams';
import user from './reducers/user';


const appReducer = combineReducers({
	access,
	patients,
	pains,
	activities,
	streams,
	user,
});

const rootReducer = (state, action) => {
	if (action.type === 'USER_SIGNED_OUT') {
		state = undefined
	}

	return appReducer(state, action)
};

export default rootReducer;