export let authSettings = {
	isAuthenticated:false,
	connection:null,
	requestingAppId: 'smartphysio_admin',
	requestedPermissions: [
		{
			streamId: 'smartphysio',
			defaultName: 'Smart physio',
			level: 'manage'
		}
	],
	// set this if you don't want a popup
	returnURL: false,
	languageCode: 'fr',
	// use the built-in auth button (optional)
	spanButtonID: 'pryv-button',
	callbacks: {
		// optional (example use case: display "loading" notice)
		initialization: function () {
			//console.log('# Auth initialized');
		},
		// optional; triggered if the user isn't signed-in yet
		needSignin: function (){
			throw 'needSignin : Not implemented'
		},
		signedIn: function () {
			throw 'signedIn : Not implemented'
		},
		refused: function (reason) {
			//console.log('# Auth refused: ' + reason);
		},
		error: function (code, message) {
			//console.log('# Auth error: ' + code + ' ' + message);
		}
	}
};

