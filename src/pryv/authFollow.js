export const authFollow = {
	requestingAppId: 'smartphysio_painsreader',
	requestedPermissions: [
		{
			streamId: 'levelPains',
			defaultName: 'Level pains',
			level: 'read'
		}
	],
	returnURL: false,
	languageCode: 'fr'
};