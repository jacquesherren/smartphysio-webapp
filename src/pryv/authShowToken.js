export const authShowToken = {
	requestingAppId: 'smartphysio_android',
	requestedPermissions: [
		{
			streamId: '*',
			level: 'manage'
		}
	],
	returnURL: false,
	languageCode: 'fr'
};