import React from 'react';
import {authSettings} from './auth';
import pryv from 'pryv';
import PropTypes from "prop-types";
import {signIn, signOut} from "../actions/UserActions";
import {createStream} from "../actions/StreamsActions";
import {connect} from "react-redux";

import {
	adminHeader,
	fetchAllActivitiesByStream,
	fetchAllPatientsByStream,
	parentStream
} from '../utils/setAuthorizationHeader'


const authUser = {
	isAuthenticated: false,
	username: '',
	token: '',
	connection: null,
	error: null,
};


class PryvAuth extends React.Component {

	state = {
		current: 'home'
	};
	onSignedIn = (connection, langCode) => {
		const authUser = this.state;
		authUser.username = connection.username;
		authUser.token = connection.auth;
		authUser.isAuthenticated = true;
		authSettings.connection = connection;
		//console.log("SignInForm.onSignedIn.connection : ", authSettings);
		this.props.signIn(authUser);

		const patientStream = {
			id: "patients",
			name: "Patients",
			parentId: "smartphysio",
		};

		this.props.createStream(patientStream);

		const activityStream = {
			id: "activities",
			name: "Activities",
			parentId: "smartphysio",
		};

		this.props.createStream(activityStream);

		if (adminHeader.headers.Authorization !== authUser.token) {
			adminHeader.headers.Authorization = authUser.token;
			fetchAllPatientsByStream.headers.Authorization = authUser.token;
			fetchAllActivitiesByStream.headers.Authorization = authUser.token;
			parentStream.headers.Authorization = authUser.token;

		}
		//console.log(adminHeader, authUser.token);


	};
	needSignin = () => {
		this.props.signOut();
	};

	constructor(props) {
		super(props);
		this.state = {...authUser};
		this.pryvDomain = 'pryv.me';
	}

	pryvAuth() {
		authSettings.callbacks.signedIn = this.onSignedIn;
		authSettings.callbacks.needSignin = this.needSignin;
		pryv.Auth.config.registerURL.host = 'reg.' + this.pryvDomain;
		pryv.Auth.setup(authSettings);

	}

	componentDidMount() {
		//console.log("Navigation.componentDidMount");
		this.pryvAuth();
	}

	render() {
		const pryvSignInButton = <span id="pryv-button">PS</span>;

		return (
			<div className=" headerUser ">
				{pryvSignInButton}
			</div>
		);

	}


}

PryvAuth.contextTypes = {
	router: PropTypes.object.isRequired
};


export default connect(null, {signIn, signOut, createStream})(PryvAuth);