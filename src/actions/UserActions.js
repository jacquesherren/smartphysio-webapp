import {setPryvUrl} from "../utils/setAuthorizationHeader";
import {USER_SIGNED_IN, USER_SIGNED_OUT} from './types';


export const userSignedIn = user => ({
	type: USER_SIGNED_IN,
	user

});

export const userSignedOut = () => ({
	type: USER_SIGNED_OUT
});

export const signIn = auth => {
	return dispatch => {
		const userAdmin = {
			username: auth.username,
			token: auth.token
		};
		localStorage.setItem("token", auth.token);
		localStorage.setItem("username", auth.username);
		setPryvUrl(auth.username);
		dispatch(userSignedIn(userAdmin));

	}
};



 export const signOut = () => dispatch => {
	 // TODO : vérifier si on peu supprimer
	 localStorage.removeItem('token');
	 localStorage.removeItem('username');
 		//setAuthorizationToken(false);
 		dispatch(userSignedOut({}));
 	};






// export function setCurrentUser(user) {
// 	return {
// 		type: SET_CURRENT_USER,
// 		user
// 	};
// }
//
// export function signOut() {
// 	return dispatch => {
// 		localStorage.removeItem('jwtToken');
// 		//setAuthorizationToken(false);
// 		dispatch(setCurrentUser({}));
// 	}
// }
//
// export const login = (credentials) => dispatch =>
// 	api.user.login(credentials).then(res => {
// 		if(res.hasOwnProperty('user')){
// 			localStorage.bookwormJWT = res.user.token;
// 			//setAuthorizationHeader(res.user.token);
// 			//dispatch(userLoggedIn(res.user));
// 		}
// 		return res;
// 	});
//
// export const login = (credentials) =>
// 		dispatch =>
// 	api.user.signin(credentials).then(res => {
// 		console.log(credentials);
// 		if(res.hasOwnProperty('_id')){
// 			const token = res.token;
// 			localStorage.setItem('jwtToken', token);
// 			setAuthorizationHeader(token);
// 			dispatch(setCurrentUser(jwtDecode(token)));
// 		}
// 		return res;
// 	});

// export function signIn(userData) {
// 	return dispatch => {
// 		console.log(userData);
// 		return axios.post('/users/signup/', userData);
// 	}
// }
//
// export function signInOld() {
// 	return dispatch => {
//
// 		var data =  {
// 			"requestingAppId": "smartphysioadmin",
// 			"requestedPermissions": [
// 				{
// 					"streamId": "patients",
// 					"defaultName": "Patients",
// 					"level": "manage"
// 				}
// 			],
// 			"returnURL": "false",
// 			"languageCode": "fr"
// 		};
// 		console.log(data);
// 		return axios.post('https://reg.pryv.me/access', data);
// 	}
// }
//
// export function userSignupRequest(userData) {
// 	return dispatch => {
// 		return axios.post('/users/signup/', userData);
// 	}
// }
//
// export function isUserExists(identifier) {
// 	return dispatch => {
// 		return axios.get(`/api/users/${identifier}`);
// 	}
// }