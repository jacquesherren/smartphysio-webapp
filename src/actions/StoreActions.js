import {CLEAR_STORE} from "./types";


export const storeCleared = () => ({
	type: CLEAR_STORE
});


export const clearStore = () => dispatch => {
	dispatch(storeCleared({}));
};