import api from "../utils/ApiRequests";
import {STREAM_CREATED, STREAM_DELETED, STREAM_UPDATED, STREAMS_FETCHED} from './types';

// data.entities.patients
const streamsFetched = streams => ({
	type: STREAMS_FETCHED,
	streams
});

const streamCreated = stream => ({
	type: STREAM_CREATED,
	stream
});

const streamDeleted = stream => ({
	type: STREAM_DELETED,
	stream

});

const streamUpdated = stream => ({
	type: STREAM_UPDATED,
	stream
});


// Event patients
export const fetchStreams = () => dispatch =>
	api.streams
		.fetchAll()
		.then(streams => dispatch(streamsFetched(streams)));


export const createStream = data => dispatch => {
	api.streams
		.create(data)
		.then(stream => dispatch(streamCreated(stream)));

};

export const deleteStream = streamId => dispatch => {
	//console.log(streamId);
	api.streams
		.delete(streamId)
		.then(stream => dispatch(streamDeleted(stream)));
};

export const updateStream = stream => dispatch => {

	api.streams
		.update(stream)
		.then(stream => dispatch(streamUpdated(stream)));
};