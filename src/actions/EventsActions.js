import api from "../utils/ApiRequests";
import {PAINS_CLEARED, PAINS_FETCHED, PATIENT_CREATED, PATIENT_DELETED, PATIENTS_FETCHED} from './types';

// set store pains
const painsFetched = pains => ({
	type: PAINS_FETCHED,
	pains
});

// clear pain store
const painsCleared = () => ({
	type: PAINS_CLEARED,
});

// add new patient
const patientCreated = patient => ({
	type: PATIENT_CREATED,
	patient

});

// delete patient id
const patientDeleted = patient => ({
	type: PATIENT_DELETED,
	patient
});

// set store patients
const patientFetched = patients => ({
	type: PATIENTS_FETCHED,
	patients
});


// Events > Pains
// connection : username & token (auth)
export const fetchPains = connection => dispatch =>
	api.pains
		.fetchAll(
			{
				username: connection.username,
				data: {
					headers: {'Authorization': connection.auth},
					params: {
						limit: 0
					},
				}
			}
		)
		.then(res => dispatch(painsFetched(res.data.events)))
		.catch(function (error, res) {
			//console.log(error, res);
		});

/**
 * Delete all pains stored in redux
 * @returns {function(*)}
 */
export const clearPains = () => dispatch => {
	dispatch(painsCleared({}));
};

// Event patients
export const fetchPatients = () => dispatch =>
	api.patients
		.fetchAll()
		.then(patients => dispatch(patientFetched(patients)));


export const createPatient = data => dispatch => {
	api.patients
		.create(data)
		.then(patient => dispatch(patientCreated(patient)));

};

export const deletePatient = id => dispatch => {
	//console.log(id);
	api.patients
		.delete(id)
		.then(patient => dispatch(patientDeleted(patient)));

};