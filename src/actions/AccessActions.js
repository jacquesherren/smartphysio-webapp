import api from "../utils/ApiRequests";
import {ACCESS_FETCHED, ACCESS_REMOVED, ACCESS_UPDATED} from "./types";


// add new patient
const accessFetched = access => ({
	type: ACCESS_FETCHED,
	access
});

const accessUpdated = access => ({
	type: ACCESS_UPDATED,
	access
});

const accessRemoved = () => ({
	type: ACCESS_REMOVED,
});


export const accessPatient = data => dispatch => {

	api.access
		.fetchAccess(data)
		.then(access => dispatch(accessFetched(access)));

};


export const updateAccessPatient = urlPoll => dispatch => {
	api.access
		.updateAccess(urlPoll)
		.then(access => dispatch(accessUpdated(access)));
};


export const removeAccessPatient = () => dispatch => {
	return dispatch(accessRemoved());
};


