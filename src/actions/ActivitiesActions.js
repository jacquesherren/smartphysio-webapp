import api from "../utils/ApiRequests";
import {ACTIVITIES_FETCHED, ACTIVITY_CREATED, ACTIVITY_DELETED, ACTIVITY_UPDATED} from './types';

// set store pains
const activitiesFetched = activities => ({
	type: ACTIVITIES_FETCHED,
	activities
});

// add new patient
const activityCreated = activity => ({
	type: ACTIVITY_CREATED,
	activity

});

// delete patient id
const activityDeleted = activity => ({
	type: ACTIVITY_DELETED,
	activity
});

// set store patients
const activityUpdated = activity => ({
	type: ACTIVITY_UPDATED,
	activity
});

// Event patients
export const fetchActivities = () => dispatch =>
	api.activities
		.fetchAll()
		.then(activities => dispatch(activitiesFetched(activities)));


export const createActivity = data => dispatch => {
	api.activities
		.create(data)
		.then(activity => dispatch(activityCreated(activity)));

};

export const deleteActivity = id => dispatch => {
	api.activities
		.delete(id)
		.then(activity => dispatch(activityDeleted(activity)));
};

export const updateActivity = id => dispatch => {
	api.activities
		.delete(id)
		.then(activity => dispatch(activityUpdated(activity)));
};