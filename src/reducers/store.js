//import {createSelector} from 'reselect';
import {CLEAR_STORE} from '../actions/types';


export default function store(state = {}, action = {}) {
	//console.log(action.data);
	switch (action.type) {
		case CLEAR_STORE:
			return undefined;
	}
};

// // SELECTORS
// export const accessSelector = state => state.access;
// export const allAccessSelector = createSelector(accessSelector, hash =>
// 	Object.values(hash)
// );