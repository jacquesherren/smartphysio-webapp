import {createSelector} from 'reselect';
import {PATIENT_CREATED, PATIENT_DELETED, PATIENTS_FETCHED} from '../actions/types';

export default function events(state = [], action = {}) {
	switch (action.type) {
		case PATIENTS_FETCHED:
			state = [];
			if (action.patients !== undefined)
				return [...state, ...action.patients];
			else
				return state;

		case PATIENT_CREATED:
			return [...state, action.patient];
		case PATIENT_DELETED:
			return state.filter(patient => patient.id !== action.patient.id);

		default:
			return state;
	}
};

// SELECTORS
export const patientsSelector = state => state.patients;
export const allPatientsSelector = createSelector(patientsSelector, patientsHash =>
	Object.values(patientsHash)
);
