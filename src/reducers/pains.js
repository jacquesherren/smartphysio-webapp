import {createSelector} from 'reselect';
import {PAINS_CLEARED, PAINS_FETCHED} from '../actions/types';

export default function events(state = [], action = {}) {
	switch (action.type) {
		case PAINS_FETCHED:
			state = [];
			if (action.pains !== undefined)
				return [...state, ...action.pains];
			else
				return state;
		case PAINS_CLEARED:
			state = []
			return state;
		default:
			return state;
	}
};

// SELECTORS
export const painsSelector = state => state.pains;
export const allPainsSelector = createSelector(painsSelector, PainHash =>
	Object.values(PainHash)
);
