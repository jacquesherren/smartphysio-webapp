import {createSelector} from 'reselect';
import {ACTIVITIES_FETCHED, ACTIVITY_CREATED, ACTIVITY_DELETED, ACTIVITY_UPDATED} from '../actions/types';

export default function events(state = [], action = {}) {
	switch (action.type) {
		case ACTIVITIES_FETCHED:
			state = [];
			if (action.activities !== undefined)
				return [...state, ...action.activities];
			else
				return state;

		case ACTIVITY_CREATED:
			return [...state, action.activity];

		case ACTIVITY_DELETED:
			return state.filter(activity => activity.id !== action.activity.id);

		case ACTIVITY_UPDATED:
			return state.filter(activity => activity.id !== action.activity.id);

		default:
			return state;
	}
};

// SELECTORS
export const activitiesSelector = state => state.activities;
export const allActivitiesSelector = createSelector(activitiesSelector, activitiesHash =>
	Object.values(activitiesHash)
);
