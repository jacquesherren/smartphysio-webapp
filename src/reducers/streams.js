import {createSelector} from 'reselect';
import {STREAM_CREATED, STREAM_DELETED, STREAM_UPDATED, STREAMS_FETCHED} from '../actions/types';

export default function streams(state = [], action = {}) {
	//console.log("reducers/streams.js : action = ", action);
	switch(action.type) {
		case STREAMS_FETCHED:
			state = [];
			if (action.streams !== undefined)
				return [...state, ...action.streams];
			else
				return state;
		case STREAM_CREATED:
			return [...state, action.stream];
		case STREAM_DELETED:
			return state.filter(stream => stream.id !== action.stream.id);
		case STREAM_UPDATED:
			return state.map((stream) => stream.id === action.stream.id ? {...stream, name: !stream.name} : stream);

		default: return state;
	}
};

// SELECTORS
export const streamsSelector = state => state.streams;
export const allStreamsSelector = createSelector(streamsSelector, StreamsHash =>
	Object.values(StreamsHash)
);
