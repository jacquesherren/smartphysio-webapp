import {createSelector} from 'reselect';
import {ACCESS_FETCHED, ACCESS_REMOVED, ACCESS_UPDATED} from '../actions/types';


export default function events(state = {}, action = {}) {
	switch (action.type) {
		case ACCESS_FETCHED:
		case ACCESS_UPDATED :
			return {...state, ...action.access};
		case ACCESS_REMOVED :
			return {};
		default:
			return state;
	}
};

// SELECTORS
export const accessSelector = state => state.access;
export const allAccessSelector = createSelector(accessSelector, hash =>
	hash
);
