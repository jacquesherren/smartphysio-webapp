import {USER_SIGNED_IN, USER_SIGNED_OUT} from '../actions/types';

export default function user(state = {}, action = {}) {
	//console.log("reducers/user.js : action = " ,action);
	switch(action.type) {
		case USER_SIGNED_IN:
			return  action.user;
		case USER_SIGNED_OUT:
			return {};
		default: return state;
	}
};