import React from 'react';

import {Route, withRouter,} from 'react-router-dom';
import SignInPage from './components/signin/SignInPage';
import HomePage from './components/pages/HomePage';
import PatientsPage from "./components/pages/PatientsPage";
import ActivitiesPage from "./components/pages/ActivitiesPage";
import AccountPage from "./components/pages/AccountPage";

import {connect} from "react-redux";
import PropTypes from "prop-types";
import GuestRoute from './routes/GuestRoute'
import UserRoute from './routes/UserRoute'
import Navigation from './components/master/Navigation';
import * as routes from './routes/routes';
import hevs from './ressources/hevs.png'
import valais from './ressources/valaisExcellence.png'

import logo from './logo.svg'
import {Layout} from 'antd';
import './App.css'

import PryvAuth from './pryv/PryvAuth'

const {Content, Header, Footer} = Layout;

const App = ({location, isAuthenticated}) => (

	<Layout>
		<Header className="ant-layout-header">
			<div><a href="./" className="App-title">
				<img className="App-logo" src={logo} alt="logo"/>
				Smartphysio
				</a>
			</div>

			<div className=" headerUser ">
				<PryvAuth isAuth={isAuthenticated}/>
			</div>
		</Header>
		<Layout style={{minHeight: "calc(98vh - 148px)"}}>
			<Navigation isAuth={isAuthenticated}/>

			<Content style={{margin: '64px 24px 0px', padding: '24px', background: '#fff', minHeight: 280}}>

			<Route
				location={location}
				path="/"
				exact component={HomePage}
			/>
			<Route
				location={location}
				path={routes.HOME}
				exact component={HomePage}
			/>
			<GuestRoute
				path={routes.SIGN_IN}
				location={location}
				component={SignInPage}
			/>
			<UserRoute
				path={routes.PATIENTS}
				location={location}
				exact component={PatientsPage}
			/>
				<UserRoute
					path={routes.ACTIVITY}
					location={location}
					exact component={ActivitiesPage}
				/>
			<UserRoute
				path={routes.ACCOUNT}
				location={location}
				exact component={AccountPage}
			/>

		</Content>

		</Layout>
		<Footer className="ant-layout-footer"
		        style={{display: 'inline-flex', justifyContent: "space-between", alignItems: "center"}}>
			<span style={{display: 'inline-flex', alignItems: "end"}}>
			<a target="_blank" rel="noopener noreferrer" href="http://www.hevs.ch">
				<img src={hevs} alt="hevs.ch" style={{height: '24px'}}/>
			</a>
			<p style={{
				fontSize: "9px",
				paddingLeft: "5px"
			}}> HES-SO Valais-Wallis • rue de la Plaine 2 • 3960 Sierre <br/>
				+41 27 606 89 11 • <a target="_blank" rel="noopener noreferrer"
				                      href="info@hevs.ch">info@hevs.ch</a> • <a target="_blank"
				                                                                rel="noopener noreferrer"
				                                                                href="www.hevs.ch">www.hevs.ch</a>
			</p>
			</span>
			<a target="_blank" rel="noopener noreferrer" href="http://www.valais-excellence.ch">
				<img src={valais} alt="valais-excellence.ch" style={{height: '48px'}}/>
			</a>
		</Footer>
	</Layout>

);

App.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired,
	location: PropTypes.shape({
		pathname: String.isRequired
	}).isRequired
};


function mapStateToProps(state) {
	return {
		isAuthenticated: !!state.user.token
	};
}

export default withRouter(connect(mapStateToProps)(App))