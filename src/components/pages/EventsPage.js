import React from 'react';

import {clearPains, fetchPains} from "../../actions/EventsActions";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {allPainsSelector} from "../../reducers/pains";
import {Avatar, Button, Icon, Radio} from "antd";


import ChartPains from "./ChartPains";
//import {setAuthorizationHeader} from "../../utils/setAuthorizationHeader";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;


const colors = [
	'#80f602',
	'#b5f602',
	'#daf602',
	'#f6f202',
	'#f6da02',
	'#f6c502',
	'#f6a502',
	'#f66402',
	'#b04802',
	'#731901',
	'#481001',
];

const renderContent = (content) => {
	const obj = <Avatar size="large" style={{backgroundColor: colors[content]}}>{content}</Avatar>;
	return obj;
};
const calculateTime = (created) => {
	const start = new Date(created);

	if (!start) {
		return null;
	}

	const date = typeof start === 'object' ? start : new Date(start);
	const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
	const today = new Date();
	const yesterday = new Date(today - DAY_IN_MS);
	const seconds = Math.round((today - start) / 1000);
	const minutes = Math.round(seconds / 60);
	const hours = Math.round(minutes / 60);
	const isToday = today.toDateString() === date.toDateString();
	const isYesterday = yesterday.toDateString() === date.toDateString();
	const isThisYear = today.getFullYear() === date.getFullYear();


	let val;

	if (seconds < 5) {
		val = 'now';
	} else if (seconds < 60) {
		val = `${ seconds } seconds ago`;
	} else if (seconds < 90) {
		val = 'about a minute ago';
	} else if (minutes < 60) {
		val = `${ minutes } minutes ago`;
	} else if (hours < 24) {
		val = `${ hours } hours ago`;
	} else if (isToday) {
		val = getFormattedDate(date, 'Today'); // Today at 10:20
	} else if (isYesterday) {
		val = getFormattedDate(date, 'Yesterday'); // Yesterday at 10:20
	} else if (isThisYear) {
		val = getFormattedDate(date, false, true); // 10. January at 10:20
	} else {
		val = getFormattedDate(date); // 10. January 2017. at 10:20
	}


	return (
		<ul>
			<span>{val}</span>
		</ul>);
};


const MONTH_NAMES = [
	'January', 'February', 'March', 'April', 'May', 'June',
	'July', 'August', 'September', 'October', 'November', 'December'
];


function getFormattedDate(date, prefomattedDate = false, hideYear = false) {
	const day = date.getDate();
	const month = MONTH_NAMES[date.getMonth()];
	const year = date.getFullYear();
	const hours = date.getHours();
	let minutes = date.getMinutes();

	if (minutes < 10) {
		// Adding leading zero to minutes
		minutes = `0${ minutes }`;
	}

	if (prefomattedDate) {
		// Today at 10:20
		// Yesterday at 10:20
		return `${ prefomattedDate } at ${ hours }:${ minutes }`;
	}

	if (hideYear) {
		// 10. January at 10:20
		return `${ day }. ${ month } at ${ hours }:${ minutes }`;
	}

	// 10. January 2017. at 10:20
	return `${ day }. ${ month } ${ year }. at ${ hours }:${ minutes }`;
}

function getTimeStamp(value) {
	let timestamp = new Date(value);
	let date = new Date(value).toUTCString();
	let time = new Date(value).toLocaleTimeString();
	return date;
	//return new Date(date * 1000).toLocaleDateString() + ;
}

function getByDay(timestamp) {
	let d = new Date(timestamp);
	return (new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0)).getTime();

}

function getWeekYear(timestamp) {
	let d = new Date(timestamp);
	let msSinceFirstWeekday = d.getDay() * 24 * 3600 * 1000 + d.getHours() * 3600 * 1000;
	let asWeek = new Date(d.getTime() - msSinceFirstWeekday);
	return (new Date(asWeek.getFullYear(), asWeek.getMonth(),
		asWeek.getDate(), 0, 0, 0, 0)).getTime();
}

function getByMonth(timestamp) {
	let d = new Date(timestamp);
	return (new Date(d.getFullYear(), d.getMonth(), 0, 0, 0, 0, 0)).getTime();
}

function groupByDay(pains) {

	return pains.reduce(function (groups, item) {
		const val = getByDay(item.time);
		groups[val] = groups[val] || [];
		groups[val].push(item);
		return groups
	}, {});
}

function groupByWeek(pains) {
	return pains.reduce(function (groups, item) {
		const val = getWeekYear(item.time);
		groups[val] = groups[val] || [];
		groups[val].push(item);
		return groups
	}, {});
}

function groupByMonth(pains) {
	return pains.reduce(function (groups, item) {
		const val = getByMonth(item.time);
		groups[val] = groups[val] || [];
		groups[val].push(item);
		return groups
	}, {});
}


function getClientDataActivity(cd) {

	if (cd !== undefined && cd != null) {
		if (cd.hasOwnProperty('spActivity')) {
			//console.log("spActivity", cd.spActivity);
			return cd.spActivity.activity;
		} else if (cd.hasOwnProperty('Activity')) {
			//console.log("nosp", cd.spActivity)
			return cd.activity;
		}
	}
	return '';
}

function getClientDataFlag(cd) {

	if (cd !== undefined && cd !== null) {
		if (cd.hasOwnProperty('spActivity')) {
			if (cd.spActivity.flag === true) {
				return 'Flagged'
			}
		} else if (cd.hasOwnProperty('activity')) {
			if (cd.activity.flag === true) {
				return 'Flagged'
			}
		}
	}
	return '';
}


class EventsPage extends React.Component {

	onInit = props => props.fetchPains(this.props.connection);

	goBack = () => {

		this.props.clearPains();
		this.props.backToPatientList();
	};

	constructor(props) {
		super(props);
		//console.log("EventsPage.constructor.props : ", props);
		this.state = {
			connection: null,
			pains: [],
			interval: "all",
			groups: []
		};

		this.onChange = this.onChange.bind(this);
	}

	componentWillUnmount() {
		this.props.clearPains();
	}

	componentDidMount() {

		const connection = this.props.connection;
		//console.log(connection);
		this.onInit(this.props);
	}

	componentWillReceiveProps(nextProps) {
		//console.log(nextProps);
		//console.log(this.state);
		//this.setState({connection: JSON.parse(JSON.stringify(nextProps.connection))});

		const painsProps = JSON.parse(JSON.stringify(nextProps.pains));
		const pains = [];

		painsProps.map((pain) => {
			const item = {
				key: pain.id,
				content: pain.content,
				clientData: pain.clientData,
				time: pain.time * 1000,
			};
			console.log(item.time);
			pains.push(
				item
			);
		});

		pains.sort(function (x, y) {
			return x.time - y.time;
		});


		console.log(pains);
		for (let n = 0; n < pains.length - 1; n++) {

			let gap = (pains[n + 1].time - pains[n].time) / 3600000;
			if (gap <= 0.25) {
				console.log('delete data :' + n, gap);
			} else if (gap > 0.25 && gap <= 4) {
				console.log('insert datas :' + n, gap);
			}
			else {
				console.log('no datas :' + n, gap);
			}

		}


		this.setState({pains: pains});
	}

	onChange(e) {
		const pains = this.state.pains;
		switch (e.target.value) {
			case 'daily' :
				this.setState({interval: 'daily'});
				const groupedDaily = groupByDay(pains);

				this.setState({groups: groupedDaily});
				break;

			case 'weekly' :
				this.setState({interval: 'weekly'});
				const groupedWeekly = groupByWeek(pains);

				this.setState({groups: groupedWeekly});
				break;

			case 'monthly' :
				this.setState({interval: 'monthly'});
				const groupedMonthly = groupByMonth(pains);

				this.setState({groups: groupedMonthly});
				break;

			default :
				this.setState({interval: 'all'});
				this.setState({groups: []});

		}
	};

	render() {
		const pains = this.state.pains;
		const groups = this.state.groups;
		const interval = this.state.interval;
		let datas = [];
		let flaggedDatas = [];
		let activityDatas = [];
		let typeLine;

		//console.log(this.state);

		switch (interval) {
			case 'daily' :

			case 'weekly' :

			case 'monthly' :
				typeLine = {
					type: 'spline',
				};
				datas = [];
				for (let key in groups) {

					let sum = 0;
					for (let i = 0; i < groups[key].length; i++) {
						sum += parseInt(groups[key][i].content, 10); //don't forget to add the base
					}

					let avg = Math.round(sum / groups[key].length * 100) / 100;


					datas.push([
						parseInt(key),
						avg,
					])

				}

				break;
			default :
				typeLine = {
					type: 'area',
					step: 'left'
				};
				pains.map((pain) => {
					datas.push([
						pain.time,
						pain.content,
					]);
					if (pain.hasOwnProperty('clientData') && pain.clientData !== undefined) {
						if (pain.clientData.hasOwnProperty('spActivity')) {
							if (pain.clientData.spActivity.flag === true) {
								if (pain.clientData.spActivity.comment !== undefined) {
									flaggedDatas.push(
										{
											x: pain.time,
											title: 'F',
											text: pain.clientData.spActivity.comment,
											fillColor: 'rgba(255,0,0,0.5)',
										},
									)
								} else {
									flaggedDatas.push(
										{
											x: pain.time,
											title: 'F',
											text: '',
											fillColor: 'rgba(255,255,255,0.8)',
										},
									)
								}
							}
						}
						if (pain.clientData.hasOwnProperty('spActivity')) {
							if (pain.clientData.spActivity.hasOwnProperty('activity')) {
								activityDatas.push(
									{
										'xAxis': 0,
										'yAxis': 0,
										'x': pain.time,
										'y': pain.content,
										'title': pain.clientData.spActivity.streamId,
										'text': pain.clientData.spActivity.activity
									},
								)
							}
						}
					}
				});
		}



		const movingAve = [];
		for (let i = 1; i < datas.length - 1; i++) {
			let mean = (datas[i][1] + datas[i - 1][1] + datas[i + 1][1]) / 3.0;
			movingAve.push([datas[i + 1][0], mean]);
		}

		let result;
		if (pains !== undefined && pains.length > 0) {
			result =
					<ChartPains data={datas} moveAvg={movingAve} flaggedData={flaggedDatas}
					            activityData={activityDatas} typeLine={typeLine}/>
			{/*<Table columns={columns} dataSource={pains} rowKey={pains.id}/>
					<p>{pains.length} events found!</p>*/
			}

		}
		else {
			result = <h3>No events</h3>
		}

		return (

			<div align="left" style={{margin: "auto", padding: "0 0 12px"}}>
				<Button type="primary" onClick={this.goBack}>
					<Icon type="left"/>Backward
				</Button>
				<RadioGroup style={{margin: "auto", marginLeft: "12px", padding: "0 0 12px"}} onChange={this.onChange}
				            defaultValue={this.state.interval}>
					<RadioButton value="all">All</RadioButton>
					<RadioButton value="daily">Daily</RadioButton>
					<RadioButton value="weekly">Weekly</RadioButton>
					<RadioButton value="monthly">Monthly</RadioButton>
				</RadioGroup>
				{result}

			</div>
		);
	}
}

EventsPage.propTypes = {
	fetchPains: PropTypes.func.isRequired,
	pains: PropTypes.arrayOf(
		PropTypes.shape({
			content: PropTypes.number.isRequired
		}).isRequired
	).isRequired
};

function mapStateToProps(state) {
	return {
		pains: allPainsSelector(state)
	};
}

export default connect(mapStateToProps, {fetchPains, clearPains})(EventsPage);

