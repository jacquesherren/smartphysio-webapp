import React from 'react';
import Highcharts from 'highcharts/highstock';
//import HighchartsReact from 'highcharts-react-official';
import HighchartsReact from 'react-highcharts/ReactHighstock'
// Highcharts exporting
import * as HighchartsExporting from "highcharts/modules/exporting";
import * as HighchartsExportingData from "highcharts/modules/export-data";


class ChartPains extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

			chart:
				{
					rangeSelector: {
						buttons: [{
							type: 'hour',
							count: 1,
							text: '1h'
						}, {
							type: 'day',
							count: 1,
							text: '1d'
						}, {
							type: 'week',
							count: 1,
							text: '1w'
						}, {
							type: 'month',
							count: 1,
							text: '1m'
						}, {
							type: 'year',
							count: 1,
							text: '1y'
						}, {
							type: 'all',
							text: 'All'
						}],
						inputEnabled: false, // it supports only days
						selected: 4 // all
					},

					chart: {
						zoomType: "x",
						height: 600,
					},
					exporting: {
						enabled: true
					},
					minorGridLineWidth: 0,
					gridLineWidth: 0,
					alternateGridColor: null,
					legend: {
						enabled: true,
						align: 'center',
						backgroundColor: '#FFFFFF',
						borderColor: 'black',
						borderWidth: 1,
						layout: 'horizontal',
						verticalAlign: 'bottom',
						y: 0,
						shadow: false
					},
					tooltip: {
						shared: true,
						split: false,


					},


					title: {
						text: 'pains'
					},
					xAxis: {
						type: 'datetime',
					},
					yAxis: {
						plotBands: [{ // Light air
							from: 0,
							to: 2.5,
							color: 'rgba(39, 174, 96,0.2)',

						}, { // Light breeze
							from: 2.5,
							to: 5,
							color: 'rgba(241, 196, 15,0.2)',

						}, { // Gentle breeze
							from: 5,
							to: 7.5,
							color: 'rgba(230, 126, 34,0.2)',

						}, { // Moderate breeze
							from: 7.5,
							to: 10,
							color: 'rgba(192, 57, 43,0.2)',

						}],
						max: 12,
						min: 0,
					},

					plotOptions: {
						series: {
							pointStart: Date.UTC(2010, 0, 1),
							pointInterval: 3600 * 250 // every quart hour
						}
					},

					series: [
						{
							id: 'painsseries',
							name: 'pains value',
							data: [],
							type: 'area',
							step: 'left',
							color: '#2980b9'
						},
						{
							name: 'moving average',
							data: [],
							color: '#34495e',
							marker: {enabled: false},
							tooltip: {
								valueDecimals: 2
							}
						},
						{
							type: 'flags',
							name: 'Flagged',
							color: '#d35400',
							fillColor: 'rgba(255,255,255,0.8)',
							data: [],
							onSeries: 'painsseries',
							shape: 'circlepin',
							width: 16
						},
						{
							id: 'activity',
							name: 'Activity',
							data: [],
							color: '#c8c8c8',
							lineWidth: 0,
							marker: {
								symbol: 'circle',
								enabled: true,
								radius: 6,
								color: '#3498db',
								states: {
									hover: {
										fillColor: '#c8c8c8',
										lineColor: '#d35400',
										lineWidth: 1
									}
								},
							},
							tooltip: {
								useHTML: true,
								pointFormat: '<span style="color:{point.color}">\u25CF</span> ' +
								'{series.name} / {point.title} : </b><b>{point.text}</b><br/>'
							},
							states: {
								hover: {
									lineWidthPlus: 0
								}
							}
						}

					],

				},
		};

	}

	componentWillReceiveProps(nextProps) {
		//console.log(nextProps);
		let chart = {...this.state.chart};

		const {data, moveAvg, flaggedData, activityData, typeLine} = nextProps;

		const series = [
			{
				id: 'painsseries',
				name: 'pains value',
				data: data,
				type: typeLine.type,
				step: typeLine.step,
				color: '#2980b9'
			},
			{
				name: 'moving average',
				data: moveAvg,
				type: 'spline',
				color: '#34495e',
				tooltip: {
					valueDecimals: 2
				}
			},
			{
				type: 'flags',
				name: 'Flagged',
				color: '#d35400',
				fillColor: 'rgba(255,255,255,0.8)',
				data: flaggedData,
				onSeries: 'painsseries',
				shape: 'circlepin',
				width: 16
			},
			{
				id: 'activity',
				name: 'Activity',
				color: '#c8c8c8',
				data: activityData,
				lineWidth: 0,
				marker: {
					symbol: 'circle',
					enabled: true,
					radius: 6,
					color: '#3498db',
					states: {
						hover: {
							fillColor: '#c8c8c8',
							lineColor: '#d35400',
							lineWidth: 1
						}
					},
				},
				tooltip: {
					useHTML: true,
					pointFormat: '<span style="color:{point.color}">\u25CF</span> ' +
					'{series.name} / {point.title} : </b><b>{point.text}</b><br/>'
				},
				states: {
					hover: {
						lineWidthPlus: 0
					}
				}
			}


		];
		chart.series = series;
		this.setState({chart});
	}

	componentDidMount() {

		let chart = {...this.state.chart};

		const {data, moveAvg, flaggedData, activityData, typeLine} = this.props;


		const series = [
			{
				id: 'painsseries',
				name: 'pains value',
				data: data,
				type: typeLine.type,
				step: typeLine.step,
				color: '#2980b9',
				pointInterval: 24 * 3600
			},
			{
				name: 'moving average',
				data: moveAvg,
				type: 'spline',
				color: '#34495e',
				tooltip: {
					valueDecimals: 2
				}
			},
			{
				type: 'flags',
				name: 'Flagged',
				color: '#d35400',
				fillColor: 'rgba(255,255,255,0.8)',
				data: flaggedData,
				onSeries: 'painsseries',
				shape: 'circlepin',
				width: 16
			},
			{
				id: 'activity',
				name: 'Activity',
				data: activityData,
				color: '#c8c8c8',
				lineWidth: 0,
				marker: {
					symbol: 'circle',
					enabled: true,
					radius: 4,
					color: '#3498db',
					states: {
						hover: {
							fillColor: '#c8c8c8',
							lineColor: '#d35400',
							lineWidth: 1
						}
					},
				},
				tooltip: {
					useHTML: true,
					pointFormat: '<span style="color:{point.color}">\u25CF</span> ' +
					'{series.name} / {point.title} : </b><b>{point.text}</b><br/>'
				},
				states: {
					hover: {
						lineWidthPlus: 0
					}
				}
			}


		];
		chart.series = series;
		this.setState({chart});

	}


	render() {
		//const data = this.props;
		//console.log(data);
		//console.log("ChartPains.render.state : ", this.state);

		const chart = this.state.chart;
		//console.log(chart);
		HighchartsExporting(Highcharts);
		HighchartsExportingData(Highcharts);
		return (
			<div>
				<HighchartsReact
					config={chart}
				/>
			</div>
		)
	}
	;
}

export default ChartPains;



