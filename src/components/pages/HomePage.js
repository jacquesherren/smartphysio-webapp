import React from "react";
import {Link} from 'react-router-dom';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import pryvLogo from '../../ressources/Pryv-logo-source.png'
import phone from '../../ressources/mobilePhone.png'
import watch from '../../ressources/mobileWatch.png'
import webApp from '../../ressources/webApp.png'
import arrowPush from '../../ressources/arrowPost.png'
import arrowGet from '../../ressources/arrowGet.png'
import hessoValaisWallis from '../../ressources/hessoValaisWallis.png'
import jhe from '../../ressources/jacquesHerren.jpg'


import {Avatar, Button, Carousel, Icon, List} from 'antd';
import * as routes from "../../routes/routes";

class HomePage extends React.Component {

	renderHome = () => {

		const {isAuthenticated, username} = this.props;

		return (

			<div>

				{isAuthenticated ? (
					<div className="homePage">
						<h1>Home Page</h1>
						<h3>Hello {username}, Welcome to smartphysio</h3>
						<p style={{textAlign: 'center', marginTop: "48px"}}>

							<Button>
								<Link to={routes.PATIENTS}><Icon type="team"/>Go to your patient's list</Link>
							</Button>
						</p>
					</div>
				) : (
					<div className="homePage">
						<h1>Home Page</h1>
						<h3>Hello guest, welcome to smartphysio</h3>

						<p style={{textAlign: 'justify', marginTop: "48px"}}>
							Un rendez-vous chez le physiothérapeute débute
							fréquemment avec la même question :<br/><br/>
							<b>« Comment allez-vous depuis la dernière fois ? »</b><br/><br/>
							Le patient plonge dans sa mémoire afin d’y trouver des
							informations, qui pourraient aider le physiothérapeute dans
							son suivi médical. L’objectif de ce projet est d’offrir un service qui permet au
							patient de <b>documenter son état de santé</b>. En parallèle une application web, permet au
							physiothérapeute
							de <b>suivre ses patients</b> et d’analyser les données afin d’adapter au mieux le
							traitement.
						</p>
						<p style={{textAlign: 'center'}}>***</p>
					</div>
				)}
			</div>
		);

	}

	renderTechnology = () => {

		const {isAuthenticated, username} = this.props;

		return (

			<div>
				<h1>Overview</h1>
				<h3>How it works ?</h3>

				<span className="test" style={{
					width: "100%",
					display: "inline-flex",
					justifyContent: "space-between",
					alignItems: "stretch",
					marginTop: "48px"
				}}>
					<span className="overview-card">
						<span>
							<h3 className="overview-card-title">Patient side</h3>
							<p className="overview-card-p">The patient gives informations about his health from mobile devices <br/>
									- Pain level <br/>
									- Activities <br/>
									- Comments <br/>
							</p>
							<a target="_blank" rel="noopener noreferrer"
							   href="https://play.google.com/store/apps/details?id=ch.hevs.tb.jackh.smartphysio_android">
							</a>
						</span>

						<span>

							<span style={{
								width: "80%",
								display: "inline-flex",
								justifyContent: "space-evenly",
								alignItems: "center"
							}}>

								<img src={watch} alt="watch" style={{width: '50%', maxWidth: '96px'}}/>
								<img src={phone} alt="phone" style={{width: '50%', maxWidth: '96px'}}/>


							</span>

							<span>
									<a target="_blank" rel="noopener noreferrer"
									   href='https://play.google.com/store/apps/details?id=ch.hevs.tb.jackh.smartphysio_android&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img
										alt='Get it on Google Play'
										src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'
										style={{height: "46px"}}/></a>

							</span>
						</span>

					</span>

					<span className="spanArrows">
						<img src={arrowPush} alt="arrowPush" style={{width: '100%'}}/>
					</span>


					<span className="overview-card">
						<span>
							<h3 className="overview-card-title">Backend</h3>

							<p className="overview-card-p">
								API and database on Pryv
							</p>
						</span>
						<span>
							<a target="_blank" rel="noopener noreferrer" href="http://pryv.com">
								<img src={pryvLogo} alt="PrYv.io" style={{padding: "24px", height: '96px'}}/>
							</a>
						</span>

					</span>


					<span className="spanArrows">
						<img src={arrowGet} alt="arrowGet" style={{width: '100%'}}/>
					</span>

					<span className="overview-card">
						<span>
						<h3 className="overview-card-title">Physiotherapist side</h3>

							<p className="overview-card-p">
							The physiotherapist analyze patients stats on the web app
							</p>
						</span>
						<span>
						<img src={webApp} alt="webApp" style={{padding: "24px", width: '100%', maxWidth: '248px'}}/>
							</span>
					</span>
				</span>

			</div>


		);

	};

	renderCredit = () => {

		const {isAuthenticated, username} = this.props;

		const data = [
			{
				description: 'Jacques Herren',
				title: 'Student',
				extra: (<Avatar src={jhe}/>)
			},
			{
				description: 'Michael Schumacher',
				title: 'Professor',
				extra: (<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>)
			},
			{
				description: 'Roger Hilfiker',
				title: 'Client',
				extra: (<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>)
			},
			{
				description: 'HES-So Valais/Wallis',
				title: 'School',
				extra: (<img src={hessoValaisWallis} alt="hevs.ch"
				             style={{width: '80%', maxWidth: '196px', minWidth: '196px'}}/>)
			},
			{
				description: 'Web App - Smartphysio',
				title: 'version 1.2.2',
				avatar: ''
			},
		];

		return (

			<div>
				<h1 style={{textAlign: "center"}}>About</h1>

				<h3 style={{textAlign: "center"}}>HES-So Valais/Wallis - Bachelor Thesis 2018</h3>

				<List
					style={{marginTop: "48px"}}
					itemLayout="vertical"
					dataSource={data}
					renderItem={item => (
						<List.Item
							extra={item.extra}
						>
							<List.Item.Meta

								description={item.description}
								title={<p>{item.title}</p>}

							/>
						</List.Item>
					)}
				/>

			</div>


		);

	}

	render() {


		return (

			<Carousel effect="scrollx">
				<div className="ant-row-flex-center" align="center"
				     style={{width: "100%", margin: "auto", paddingTop: "10px"}}>
					{this.renderHome()}
				</div>
				<div className="ant-row-flex-center" align="center"
				     style={{width: "100%", margin: "auto", paddingTop: "10px"}}>
					{this.renderTechnology()}
				</div>
				<div id="homeAbout"
				     style={{width: "100%", margin: "auto", paddingTop: "10px"}}>
					{this.renderCredit()}
				</div>

			</Carousel>


		);
	}
}


HomePage.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
	return {
		isAuthenticated: !!state.user.token,
		username: state.user.username,
		token: state.user.token,
	};
}

export default connect(mapStateToProps)(HomePage);
