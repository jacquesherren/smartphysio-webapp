import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";


class AccountPage extends React.Component {

	render() {

		const {isAuthenticated} = this.props;


		return (

			<div className="article">
				<h1>Account page</h1>
				{isAuthenticated ? (
					<div>
						<h3>Welcome to smartphysio</h3>
						<p>Username : {this.props.username} </p>
						<p>token : {this.props.token} </p>
					</div>
				) : (
					<div>
						<p>Please sign in to connect to Smartphysio.</p>

					</div>
				)}
			</div>
		)
	}
}

AccountPage.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
	return {
		isAuthenticated: !!state.user.token,
		username: state.user.username,
		token: state.user.token,
	};
}

export default connect(mapStateToProps)(AccountPage);
