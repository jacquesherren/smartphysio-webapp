import React from 'react';
import ActivitiesList from './ActivitiesList'

class ActivitiesPage extends React.Component {


	render() {

		return (
			<div>
				<h1>Manage activity</h1>
				< ActivitiesList/>
			</div>
		)
	}

}

export default ActivitiesPage