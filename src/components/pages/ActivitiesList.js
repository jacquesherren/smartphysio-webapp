import React from 'react';
import {Button, Icon, Input, Popconfirm, Table} from 'antd';
import PropTypes from "prop-types";
import {createStream, deleteStream, fetchStreams, updateStream} from "../../actions/StreamsActions";
import {createActivity, deleteActivity, fetchActivities} from "../../actions/ActivitiesActions";
import {allStreamsSelector} from "../../reducers/streams";
import {allActivitiesSelector} from "../../reducers/activities";
import WrappedActivityModalForm from '../forms/ActivityModalForm'

import {connect} from "react-redux";

function getTimeStamp(value) {
	let date = new Date(value);
	if (isValidDate(date)) {
		return date.toUTCString();
	}
	return null;
}

function isValidDate(d) {
	return d instanceof Date && !isNaN(d);
}


class EditableCell extends React.Component {
	state = {
		value: this.props.value,
		editable: false,
	};

	handleChange = (e) => {
		const value = e.target.value;
		this.setState({value});
	};

	check = () => {
		this.setState({editable: false});
		if (this.props.onChange) {
			this.props.onChange(this.state.value);
		}
	};

	edit = () => {
		this.setState({editable: true});
	};

	render() {
		const {value, editable} = this.state;

		return (
			<div className="editable-cell">
				{
					editable ? (
						<Input
							value={value}
							onChange={this.handleChange}
							onPressEnter={this.check}
							placeholder="Give a group name"
							suffix={(
								<Icon
									type="check"
									className="editable-cell-icon-check"
									onClick={this.check}
								/>
							)}
						/>
					) : (
						<div style={{paddingRight: 24}}>
							{value || ' '}
							<Icon
								type="edit"
								className="editable-cell-icon"
								onClick={this.edit}
							/>
						</div>
					)
				}
			</div>
		);
	}
}

class ActivitiesList extends React.Component {

	state = {
		dataSource: [{
			key: '',
			name: ''
		}],
		expendedData: [{
			key: '',
			name: ''
		}],
		count: 0,
		visible: false,
	};

	expandedRowRender = value => {

		const selectedStream = value;
		this.selectedStream = selectedStream;
		const columns = [
			{title: 'Name', width: '70%', dataIndex: 'name', key: 'name'},
			{
				title: 'Action',
				dataIndex: 'operation',
				width: '30%',
				key: 'operation',
				render: (text, record) => {

					if (record.key !== 0) {
						return <Popconfirm title="Sure to delete?" onConfirm={() => this.onDeleteActivity(record)}>
							<a>Delete</a>
						</Popconfirm>
					}
					else {
						return <Button onClick={this.showModal} type="default" style={{margin: 'auto'}}>
							Add a new activity
						</Button>
					}
				},
			},
		];

		const data = [];
		this.state.expendedData.map((activity) => {
			if (value.key === activity.streamId) {
				const item = {
					key: activity.key,

					streamId: activity.streamId,
					name: activity.name,
				};
				data.push(
					item
				);
			}
		});

		const item = {
			key: 0,

			streamId: value.key,
			name: '',
		};
		data.push(
			item
		);

		return (
			<Table
				columns={columns}
				dataSource={data}
				pagination={false}
			/>
		);
	};

	onInit = props => {
		props.fetchStreams();
		props.fetchActivities();
	};

	onCellChange = (key, dataIndex) => {
		return (value) => {
			const dataSource = [...this.state.dataSource];
			//console.log(dataSource);
			const target = dataSource.find(item => item.key === key);
			if (target) {
				target[dataIndex] = value;
				this.setState({dataSource});

				// console.log(dataSource);
				// console.log(target);
				// console.log(value);

				// Create a new activity (id/key = 0)
				if (target.key === 0) {
					const stream = {
						//id: id,
						name: value,
						parentId: "activities"
					};
					//console.log(stream);
					this.props.createStream(stream);
				}
				// Update if activity already exist
				else {
					const stream = {
						id: target.key,
						update: {
							name: value,
						}
					};
					this.props.updateStream(stream);
				}
			}
		};
	};

	onDelete = (record) => {
		this.props.deleteStream(record.key);
	};
	columns = [{
		title: 'Activity group',
		dataIndex: 'name',
		width: '70%',
		key: 'id',
		render: (text, record) => (
			<EditableCell
				value={text}
				onChange={this.onCellChange(record.key, 'name')}
			/>
		),
	}, {
		title: 'operation',
		dataIndex: 'operation',
		key: 'operation',
		render: (text, record) => {
			let count = 0;
			this.state.expendedData.map((activity) => {
				if (record.key === activity.streamId) {
					count++;
				}
			});

			if (count === 0) {
				return <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record)}>
					<a>Delete</a>
				</Popconfirm>
			}
			else {
				return null;
			}
		},
	}];
	onDeleteActivity = (record) => {
		this.props.deleteActivity(record.key);
	};
	handleAdd = () => {
		const {count, dataSource} = this.state;
		const newData = {
			key: count,
			name: '',
		};


		//this.props.createStream(patient);
		this.setState({
			dataSource: [...dataSource, newData],
			count: count + 1,
		});

		//console.log(newData);
	};
	showModal = () => {
		this.setState({visible: true});
	};

	handleCancel = () => {
		this.setState({visible: false});
	};

	handleCreate = () => {
		const streamId = this.selectedStream.key;
		const form = this.formRef.props.form;
		//console.log(form);
		form.validateFields((err, values) => {
			//console.log(values);
			if (err) {
				return;
			}
			const activity = {
				streamId: streamId,
				type: 'note/text',
				content: values.name,
			};

			//console.log('Received values of form: ', values, activity);
			form.resetFields();
			this.setState({visible: false});
			//console.log(activity);
			this.props.createActivity(activity);

		});
	};

	saveFormRef = (formRef) => {
		//console.log(formRef);
		this.formRef = formRef;
	};


	componentDidMount() {
		this.onInit(this.props);
	}

	constructor(props) {
		super(props);
		this.selectedStream = '';
	}

	componentWillReceiveProps(nextProps) {
		const streams = JSON.parse(JSON.stringify(nextProps.streams));
		const activities = JSON.parse(JSON.stringify(nextProps.activities));
		//console.log(nextProps);

		const dataSource = [];
		streams.map((stream) => {
			const item = {
				key: stream.id,
				name: stream.name,

			};
			dataSource.push(
				item
			)
		});

		this.setState({dataSource: dataSource});

		const expendedData = [];
		activities.map((activity) => {
			const item = {
				key: activity.id,

				streamId: activity.streamId,
				name: activity.content,


			};
			expendedData.push(
				item
			)
		});

		this.setState({expendedData: expendedData});
	}

	render() {
		const {dataSource} = this.state;
		//console.log(this.state);
		if (dataSource !== undefined && dataSource.length > 0) {

		}

		return (
			<div>
				<Button onClick={this.handleAdd} type="primary" style={{marginBottom: 16}}>
					Create a new activity group
				</Button>

				<WrappedActivityModalForm
					data={this.selectedStream}
					wrappedComponentRef={this.saveFormRef}
					visible={this.state.visible}
					onCancel={this.handleCancel}
					onCreate={this.handleCreate}
				/>

				<Table className="components-table-demo-nested" dataSource={dataSource} columns={this.columns}
				       expandedRowRender={this.expandedRowRender}/>
			</div>
		);

	}
}

ActivitiesList.propTypes = {
	fetchStreams: PropTypes.func.isRequired,
	fetchActivities: PropTypes.func.isRequired,
	createStream: PropTypes.func.isRequired,
	updateStream: PropTypes.func.isRequired,
	createActivity: PropTypes.func.isRequired,
	deleteActivity: PropTypes.func.isRequired,
	streams: PropTypes.array.isRequired,
	activities: PropTypes.array.isRequired,
};


function mapStateToProps(state) {
	return {
		streams: allStreamsSelector(state),
		activities: allActivitiesSelector(state)
	};
}

export default connect(mapStateToProps, {
	fetchStreams,
	deleteStream,
	createStream,
	updateStream,
	fetchActivities,
	createActivity,
	deleteActivity
})(ActivitiesList);
