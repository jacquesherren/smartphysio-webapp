import React from 'react';

import {deletePatient, fetchPatients} from "../../actions/EventsActions";

import {connect} from "react-redux";
import PropTypes from "prop-types";
import {allPatientsSelector} from "../../reducers/patients";
import {Avatar, Button, Icon, Input, Popconfirm, Table} from "antd";
import PatientFollow from './PatientFollow'

const renderContent = (content) => {
	// console.log(content);
	const initial = getFirstLetter(content);
	const obj = <Avatar size="large">{initial}</Avatar>
	return obj;
};

const emptyMessage = (
	<p>There are no patients available.</p>
);


function getFirstLetter(string) {
	return string.charAt(0).toUpperCase();
}


function getTimeStamp(value) {
	return new Date(value * 1000).toUTCString();
}


class PatientList extends React.Component {

	constructor(props) {
		super(props);
		this.state = {

			searchText: "",
			patientsFiltered: [],
			patients: [],
			filterDropdownVisible: false,
			filtered: false,

		};

		this.onOpen = this.onOpen.bind(this);

	}

	onInit = props => props.fetchPatients();

	onDelete = (record) => {
		// console.log("onDelete", record);
		this.props.deletePatient(record.id);
	};

	onInputChange = (e) => {
		this.setState({searchText: e.target.value});
	};
	onSearch = () => {
		const {searchText, patients} = this.state;

		const reg = new RegExp(searchText, 'gi');
		this.setState({
			filterDropdownVisible: false,
			filtered: !!searchText,
			patientsFiltered: patients.map((record) => {
				const match = record.content.match(reg);
				if (!match) {
					return null;
				}
				return {
					...record,
				};
			}).filter(record => !!record),
		});
	};

	componentDidMount() {
		this.onInit(this.props);

	}

	static renderFollowPatientBySharedSlice() {
		return (
			<PatientFollow btnLabel={'Follow patient by shared slice'} onAction={'FOLLOW_SHARED'}/>
		)
	}

	componentWillReceiveProps(nextProps) {
		this.setState({patients: JSON.parse(JSON.stringify(nextProps.patients))});
		this.setState({patientsFiltered: JSON.parse(JSON.stringify(nextProps.patients))});

	}

	onOpen(patient) {
		this.props.setPatient(patient.id, patient.content, patient.clientData.smartphysio_admin.auth);

	}

	renderShowToken = () => {
		return (
			<PatientFollow key={0} btnLabel={'Show watch token'} onAction={'SHOW_TOKEN'}/>
		)
	};
	renderFollowPatientByLogin = () => {
		return (
			<PatientFollow key={1} btnLabel={'Add patient'} onAction={'FOLLOW_LOGIN'}/>
		)
	};

	render() {

		const {patients, patientsFiltered} = this.state;

		let patientsTable = <div/>;
		if (patients !== undefined && patients.length > 0) {

			/**
			 * Defined table columns
			 * @type {*[]}
			 */
			const columns = [
				{title: 'Initial', dataIndex: 'content', key: 'initial', render: renderContent},
				{
					title: 'Username',
					dataIndex: 'content',
					key: 'id',
					render: (text, record) => <span onClick={() => this.onOpen(record)}><a>{text}</a></span>,
					sorter: (a, b) => a.content.localeCompare(b.content),
					filterDropdown: (
						<div className="custom-filter-dropdown">
							<Input
								ref={ele => this.searchInput = ele}
								placeholder="Search name"
								value={this.state.searchText}
								onChange={this.onInputChange}
								onPressEnter={this.onSearch}
							/>
							<Button type="primary" onClick={this.onSearch}>Search</Button>
						</div>
					),
					filterIcon: <Icon type="search" style={{color: this.state.filtered ? '#108ee9' : '#aaa'}}/>,
					filterDropdownVisible: this.state.filterDropdownVisible,
					onFilterDropdownVisibleChange: (visible) => {
						this.setState({
							filterDropdownVisible: visible,
						}, () => this.searchInput && this.searchInput.focus());
					},
				},
				{title: 'Created', dataIndex: 'created', key: 'created', render: getTimeStamp},
				{
					title: 'Action', dataIndex: '', key: 'x', render: (text, record) => {

						return (
							<span>
								<Popconfirm title="Sure to unfollow ?" onConfirm={() => this.onDelete(record)}>
									<a>Unfollow</a>
								</Popconfirm>
							</span>
						);
					},
				}
			];

			patientsTable = <div>

				<Table rowKey={record => record.id} columns={columns} dataSource={patientsFiltered}
				       key={patientsFiltered.id}/>
				<p>{patientsFiltered.length} patients found!</p>
			</div>
		}

		// render :
		//          2 buttons (follow and ShowToken)
		//          Display Patient's table if not empty
		return (
			<span>
				<div style={{display: 'flex', justifyContent: 'space-between'}}>
					{this.renderFollowPatientByLogin()}
					{this.renderShowToken()}
				</div>
				{patientsFiltered.length === 0 ? emptyMessage : patientsTable}
			</span>
		);
	}
}

PatientList.propTypes = {
	fetchPatients: PropTypes.func.isRequired,
	patients: PropTypes.array.isRequired,
};


function mapStateToProps(state) {
	return {
		patients: allPatientsSelector(state)
	};
}

export default connect(mapStateToProps, {
	fetchPatients,
	deletePatient

})(PatientList);

