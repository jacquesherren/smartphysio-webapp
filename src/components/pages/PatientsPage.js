import React from 'react';
import PatientList from './PatientList'
import EventsPage from './EventsPage'

import pryv from 'pryv'


class PatientsPage extends React.Component {

	display = (obj) => {
		//console.log(obj);
	};

	setPatient = (id, username, auth) => {

		//console.log(id, username, auth);

		const credentials = {
			username: username,
			auth: auth
		};

		const connection = new pryv.Connection(credentials);
		connection.accessInfo(function (err, info) {
			//console.log(err, info);
		});

		this.setState({
			userClicked: id,
			connection: connection,
			pages: this.pages[1],
		});
		//console.log('this.setState', this.state)
	};

	deletePatient = (id) => {
		//console.log("Ready to delete", id);
	};


	backToPatientList = () => {
		this.setState(
			{
				userClicked: "",
				connection: "",
				pages: this.pages[0],
			}
		)
	};

	constructor(props) {
		super(props);

		this.pages = [
			{
				"Name": "Patients list",
				"ActiveComponent": React.createFactory(() => <PatientList setPatient={this.setPatient}
				                                                          deletePatient={this.deletePatient}/>)
			},
			{
				"Name": "Pains",
				"ActiveComponent": React.createFactory(() => <EventsPage connection={this.state.connection}
				                                                         backToPatientList={this.backToPatientList}/>)
			}
		];


		this.state = {
			userClicked: "",
			connection: "",
			pages: this.pages[0],
		};
		//console.log("PatientsPage.constructor.props", props);
	}

	componentDidMount() {


	}

	render() {

		return (

			<span className="ant-layout-content">
				{this.state.pages.ActiveComponent()}
			</span>
		);
	}
}

// PatientsPage.propTypes = {
// 	fetchStreams: PropTypes.func.isRequired,
// 	streams: PropTypes.arrayOf(
// 		PropTypes.shape({
// 			name: PropTypes.string.isRequired
// 		}).isRequired
// 	).isRequired
// };

// function mapStateToProps(state) {
// 	return {
// 		streams: allStreamsSelector(state)
// 	};
// }

export default PatientsPage;

