import React from "react";
import PropTypes from "prop-types";
import {Button, message} from 'antd';
import {connect} from "react-redux";
import {createPatient} from "../../actions/EventsActions";
import {accessPatient, removeAccessPatient, updateAccessPatient} from "../../actions/AccessActions";
import {allAccessSelector} from "../../reducers/access";
import WrappedPatientCreateForm from '../forms/PatientModalForm'
import pryv from 'pryv'
import {authFollow} from "../../pryv/authFollow";
import {authShowToken} from "../../pryv/authShowToken";


class PatientFollow extends React.Component {

	popupCallBack = (url) => {
		this.props.updateAccessPatient(url);
	};
	accessPatient = (auth) => {
		this.setState({patientAccess: {}});
		this.props.accessPatient(auth);

	};
	showModal = () => {
		this.setState({visible: true});
	};
	handleCancel = () => {
		this.setState({visible: false});
		this.props.removeAccessPatient();
	};
	handleCreate = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			const patient = {
				streamId: 'patients',
				type: 'note/text',
				content: values.username,
				clientData: {
					smartphysio_admin: {auth: values.auth}
				},
			};
			//console.log('Received values of form: ', values);
			form.resetFields();
			this.setState({visible: false});

			const credentials = {
				username: values.username,
				auth: values.auth
			};

			const connection = new pryv.Connection(credentials);
			connection.accessInfo((err, info) => {
				if (!err) {
					//console.log(info);
					//message.success('Patient available...');
					this.props.createPatient(patient);
					message.success('Patient successfully added.');
				} else {
					switch (err.id) {
						case 'API_UNREACHEABLE' :
							message.error('This patient may not exist or you do not have permissions.', 5);
							break;
						default :
							message.error('Unknown error.');
							break;
					}
				}
			});

			this.props.removeAccessPatient();
		});
	};
	onClickAction = () => {
		//console.log(this.onAction);

		this.setState({mode: this.onAction});

		switch (this.onAction) {

			case 'FOLLOW_LOGIN' :
				this.accessPatient(authFollow);
				break;

			case 'SHOW_TOKEN' :
				this.accessPatient(authShowToken);
				break;
		}

		this.setState({
			isVisible: false,
		})
	};
	generateModal = (visible) => {
		switch (this.onAction) {
			case 'SHOW_TOKEN' :
				return (<WrappedPatientCreateForm
					wrappedComponentRef={this.saveFormRef}
					visible={visible}
					title='Show token'
					onCancel={this.handleCancel}
					onCreate={this.handleCancel}
					username={this.state.patientAccess.username}
					auth={this.state.patientAccess.token}
					mode={'READ'}
				/>)


			case 'FOLLOW_LOGIN' :
				return (<WrappedPatientCreateForm
					wrappedComponentRef={this.saveFormRef}
					visible={visible}
					title='Follow'
					onCancel={this.handleCancel}
					onCreate={this.handleCreate}
					username={this.state.patientAccess.username}
					auth={this.state.patientAccess.token}
					mode={'EDIT'}

				/>);

		}
	};
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	};

	constructor(props) {
		super(props);
		this.state = {
			intervalId: 0,
			visible: false,
			mode: '',

			patientAccess: {
				code: 0,
				status: '',
				key: '',
				poll: '',
				poll_rate_ms: 0,
				requestedPermissions: [],
				requestingAppId: '',
				url: '',
				auth: '',
				username: ''
			}

		};

		this.onAction = this.props.onAction;
		//this.mode = this.props.mode;

		//console.log(this.props);


		this.showModal = this.showModal.bind(this);
		this.accessPatient = this.accessPatient.bind(this);
		this.popupCallBack = this.popupCallBack.bind(this);

	}

	componentDidMount() {
		let intervalId = setInterval(this.timer, 1000);
		this.setState({intervalId: intervalId});
		this.setState({mode: ''});


	}

	componentWillUnmount() {
		clearInterval(this.state.intervalId);
		this.setState({mode: ''});
	}

	componentWillReceiveProps(nextProps) {
		//console.log(nextProps);

		//console.log(this.state.mode);

		if (this.state.mode === nextProps.onAction) {
			//console.log("CATCH " + this.state.mode);
			this.setState({
				visible: nextProps.visible,
				patientAccess: nextProps.access
			});
		}
	}

	render() {
		const {patientAccess} = this.state;

		if (patientAccess.code === 201 && patientAccess.url !== undefined && patientAccess.url.startsWith("https://sw.pryv.me/access/")) {

			const win = window.open(patientAccess.url, 'pryv sign in', 'width=800,height=600,status=0,toolbar=0');
			let timer = setInterval(() => {
				if (win.closed) {
					clearInterval(timer);

					function processUserInput(callback) {
						//console.log("inside processUserInput");
						callback(patientAccess.poll);
					};
					processUserInput(this.popupCallBack);
				}
			}, 1000);
		}

		let displayModal;
		displayModal = parseInt(patientAccess.code) === 200;

		//console.log(displayModal);

		let modal;

		switch (this.state.mode) {
			case 'SHOW_TOKEN' :
				//console.log('TOKEN_SHOWN');
				modal = this.generateModal(displayModal);
				break;

			case 'FOLLOW_LOGIN' :
				//console.log('LOGIN');
				modal = this.generateModal(displayModal);
				break;
		}


		return (
			<div style={{margin: "0 12px 0 0", padding: "0 0 12px"}}>

				<Button type="primary" onClick={this.onClickAction}>{this.props.btnLabel}</Button>

				{modal}
			</div>
		);
	}
}

PatientFollow.propTypes = {
	createPatient: PropTypes.func.isRequired,
	removeAccessPatient: PropTypes.func.isRequired,
	btnLabel: PropTypes.string.isRequired,
	onAction: PropTypes.string.isRequired
};

function mapStateToProps(state) {
	//console.log(state)
	return {
		access: allAccessSelector(state)
	};
}

export default connect(mapStateToProps, {
	createPatient,
	accessPatient,
	updateAccessPatient,
	removeAccessPatient
})(PatientFollow);
