import React from 'react';
import {Link} from 'react-router-dom';
import {Icon, Layout, Menu} from 'antd';
import * as routes from '../../routes/routes';

import {authSettings} from '../../pryv/auth';
import pryv from 'pryv';

import PropTypes from "prop-types";
import {signIn, signOut} from "../../actions/UserActions";
import {connect} from "react-redux";


const {Sider} = Layout;



class Navigation extends React.Component {

	state = {
		current: 'home'
	};

	handleClick = (e) => {

		this.setState({
			current: e.key,
		});
		//console.log(e);
		switch (e.key) {
			case 'home' :

				break;

			case 'account' :

				break;

			case 'patients' :

				break;
			case 'signinpage' :

				break;
			case 'signout' :
				pryv.Auth.logout(authSettings);
				this.props.signOut();
				break;
			default :

				break;
		}
	};


	render() {

		let siderMenu;
		if (!!this.props.isAuth) {
			siderMenu = (

				<Menu
					theme="dark"
					onClick={this.handleClick}
					selectedKeys={[this.state.current]}
					style={{height: '100%', borderRight: 0}}
					mode="inline">
					<Menu.Item key="home">
						<Link to={routes.HOME}><Icon type="home"/>Home</Link>

					</Menu.Item>
					<Menu.Item key="patients">
						<Link to={routes.PATIENTS}><Icon type="team"/>Patients</Link>
					</Menu.Item>
					<Menu.Item key="activities">
						<Link to={routes.ACTIVITY}><Icon type="profile"/>Activities</Link>
					</Menu.Item>
					<Menu.Item key="account">
						<Link to={routes.ACCOUNT}><Icon type="info-circle"/>Account</Link>
					</Menu.Item>
					<Menu.Item key="signout">
						<Icon type="logout"/>Signout
					</Menu.Item>
				</Menu>


			)
		}
		else {
			siderMenu = (
				<Menu theme="dark"
				      style={{height: '100%', borderRight: 0}}
				      onClick={this.handleClick}
				      selectedKeys={[this.state.current]}
				      mode="inline">
					<Menu.Item key="home">
						<Link to={routes.HOME}>Home</Link>
					</Menu.Item>
					<Menu.Item key="signinpage">
						<Link to={routes.SIGN_IN}>Sign In</Link>
					</Menu.Item>
				</Menu>


			)
		}
		return (

			<Sider width={200} style={{background: '#fff'}}
			       breakpoint="lg"
			       collapsedWidth="0"
			       onCollapse={(collapsed, type) => {
				       //console.log(collapsed, type);
			       }}>

				{siderMenu}
			</Sider>


		);

	}
}

Navigation.contextTypes = {
	router: PropTypes.object.isRequired
};


export default connect(null, {signIn, signOut})(Navigation);