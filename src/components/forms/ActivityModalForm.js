import React from "react";
import {Button, Form, Icon, Input, Modal} from 'antd';

const FormItem = Form.Item;

class ActivityModalForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			name: '',
			streamId: '',
		};

		this.handleInputChange = this.handleInputChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {

		//console.log(nextProps.data);
		this.setState({streamId: nextProps.data.key});
	}


	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});

	}


	render() {
		const {visible, onCancel, onCreate, form, data} = this.props;
		const {getFieldDecorator} = form;
		return (
			<Modal
				visible={visible}
				title="Create a new activity"
				okText="Create"
				footer={[
					<Button key="back" onClick={onCancel}>Cancel</Button>,
					<Button key="submit" type="primary" onClick={onCreate}>
						Create
					</Button>,
				]}
			>
				<div style={{margin: 'auto', padding: 6, display: 'inline-flex'}}>
					<p>Active group :</p><h4 style={{marginLeft: '.5em'}}>{data.name}</h4>
				</div>
				<Form layout="vertical">

					<FormItem>
						{getFieldDecorator('name', {
							rules: [{required: true, message: 'Please give an activity name'}],
						})(
							<Input id="name"
							       name="name"
							       placeholder="Activity name"
							       onChange={this.handleInputChange}
							       prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
							/>
						)}
					</FormItem>
				</Form>
			</Modal>
		);
	}
}


const WrappedActivityModalForm = Form.create()(ActivityModalForm);
export default (WrappedActivityModalForm);

