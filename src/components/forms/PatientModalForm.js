import React from "react";
import {Button, Form, Icon, Input, Modal} from 'antd';


const FormItem = Form.Item;

class PatientCreateForm extends React.Component {

	constructor(props) {
		super(props);


		this.handleInputChange = this.handleInputChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		//console.log(nextProps);
//		this.setState({username: nextProps.username});
//		this.setState({auth: nextProps.auth});
	}

	handleInputChange(event) {
		const target = event.target;

		const value = target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});

	}

	renderFooter() {
		//console.log(this.props.mode)
		switch (this.props.mode) {
			case 'EDIT' :
				return ([
					<Button key="back" onClick={this.props.onCancel}>Cancel</Button>,
					<Button key="submit" type="primary" onClick={this.props.onCreate}>
						Follow
					</Button>
				]);
			case 'READ' :
				return ([
					<Button key="submit" type="primary" onClick={this.props.onCancel}>
						Close
					</Button>
				]);


		}


	}


	render() {
		const {visible, onCancel, onCreate, form, username, auth, mode, title} = this.props;

		//console.log(this.props);

		const {getFieldDecorator} = form;
		return (
			<Modal
				visible={visible}
				title={title}
				okText="Create"
				footer={this.renderFooter()}
			>
				<Form layout="vertical">

					<FormItem>
						{getFieldDecorator('username', {
							initialValue: username,
							rules: [{required: true, message: 'Please input/paste the patient username!'}],
						})(
							<Input id="username"
							       name="username"

							       placeholder="Patient username"
							       onChange={this.handleInputChange}
							       prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
							/>
						)}
					</FormItem>


					<FormItem>
						{getFieldDecorator('auth', {
							initialValue: auth,
							rules: [{required: true, message: 'Please input/paste the patient token!'}],
						})(
							<Input id="auth"
							       name="auth"

							       placeholder="Patient token"
							       onChange={this.handleInputChange}
							       prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}

							/>
						)}
					</FormItem>
				</Form>
			</Modal>
		);
	}
}


const WrappedPatientCreateForm = Form.create()(PatientCreateForm);
export default (WrappedPatientCreateForm);

