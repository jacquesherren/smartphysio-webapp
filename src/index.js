import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from "react-router-dom";
import 'antd/dist/antd.css'
import {applyMiddleware, createStore} from "redux";
import {Provider} from "react-redux";
import thunkMiddleware from "redux-thunk";
import promise from "redux-promise";

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import rootReducer from "./rootReducer";
import {userSignedIn} from "./actions/UserActions";
import {composeWithDevTools} from "redux-devtools-extension";

const store = createStore(
	rootReducer,
	composeWithDevTools(
		applyMiddleware(thunkMiddleware, promise)
	)
);


if (localStorage.username !== undefined && localStorage.token !== undefined) {

	const token = localStorage.getItem('token');
	const username = localStorage.getItem('username');

	const userAdmin = {
		username: username,
		token: token
	};

//	setAuthorizationHeader(payload);
	store.dispatch(userSignedIn(userAdmin));

} else {

	//console.log("no credentials !!!! ")

}

ReactDOM.render(

	<BrowserRouter>
		<Provider store={store}>
			<Route component={App} />
		</Provider>
	</BrowserRouter>

	, document.getElementById('root')
);

registerServiceWorker();


