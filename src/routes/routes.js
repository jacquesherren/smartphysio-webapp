export const PATIENTS = '/patients';
export const SIGN_IN = '/signin';
export const HOME = '/home';
export const ACCOUNT = '/account';
export const ACTIVITY = '/activities';
