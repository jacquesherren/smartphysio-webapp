import axios from "axios";
import {USER_SIGNED_IN} from '../actions/types';
import {adminHeader, fetchAllActivitiesByStream, fetchAllPatientsByStream, parentStream} from './setAuthorizationHeader'

axios.defaults.headers.common = {
	"Content-Type": "application/json"
};


export default {
	pains: {
		fetchAll: filter => axios.get("https://" + filter.username + ".pryv.me/events", filter.data).then(res => res),
	},
	patients: {
		fetchAll: () => axios.get("/events", fetchAllPatientsByStream).then(res => res.data.events), //tested
		create: data => axios.post("/events", data, adminHeader).then(res => res.data.event),
		delete: id => axios.delete("/events/" + id, adminHeader).then(res => res.data.event),
	},
	activities: {
		fetchAll: () => axios.get("/events", fetchAllActivitiesByStream).then(res => res.data.events),  //tested
		create: data => axios.post("/events", data, adminHeader).then(res => res.data.event),
		delete: id => axios.delete("/events/" + id, adminHeader).then(res => res.data.event),
		update: id => axios.put("/events/" + id, adminHeader).then(res => res.data.event),
	},
	streams: {
		fetchAll: () => axios.get("/streams", parentStream).then(res => res.data.streams),  //tested
		create: data => axios.post("/streams", data, adminHeader).then(res => res.data.stream),
		delete: id => axios.delete("/streams/" + id, adminHeader).then(res => res.data.stream),
		update: data => axios.put("/streams/" + data.id, data.update, adminHeader).then(res => res.data.stream),
	},
	access: {
		fetchAccess: data => axios.post("https://reg.pryv.me/access", data).then(res => res.data),
		updateAccess: urlPoll => axios.get(urlPoll).then(res => res.data),
	},

	users: {
		signIn: data => {
			return dispatch => {
				axios
					.post('https://reg.pryv.me/access', {data}).then((res) => {
						dispatch({ type: USER_SIGNED_IN, payload: res });
					})
					.catch((err) => {
						console.error(err);
					});
			};
		},
	}
}

