/**
 * @jest-environment node
 */


import axios from "axios";


export const fetchAllPatientsByStream = {
	headers: {
		'Content-Type': 'application/json;charset=UTF-8',
		"Access-Control-Allow-Origin": "*",
		"Authorization": 'cjj689m010q2l0cd3q2h6lr54',
	},
	params: {
		limit: 0,
		streams: ["patients"]
	}
};

export const fetchAllActivitiesByStream = {
	headers: {
		'Content-Type': 'application/json;charset=UTF-8',
		"Access-Control-Allow-Origin": "*",
		"Authorization": 'cjj689m010q2l0cd3q2h6lr54',
	},
	params: {
		limit: 0,
		streams: ["activities"]
	}
};

export const parentStream = {
	headers: {
		'Content-Type': 'application/json;charset=UTF-8',
		"Access-Control-Allow-Origin": "*",
		"Authorization": 'cjj689m010q2l0cd3q2h6lr54',
	},


	params: {
		limit: 0,
		parentId: "activities"
	}
};


/* Jest test of axios XHR calls */
describe('axios', function () {
	let axios;

	beforeEach(function () {
		axios = require('axios');
	});

	it('fetch all patients', function () {
		let reqP = axios
			.get('https://spadmin.pryv.me/events', fetchAllPatientsByStream)
			.then(function (res) {
				expect(res.status).toBe(200);
			});
		return reqP;
	});

	it('fetch all activities', function () {
		let reqA = axios
			.get('https://spadmin.pryv.me/events', fetchAllActivitiesByStream)
			.then(function (res) {
				expect(res.status).toBe(200);
			});
		return reqA;
	});

	it('fetch all activties groups', function () {
		let reqG = axios
			.get('https://spadmin.pryv.me/streams', parentStream)
			.then(function (res) {
				expect(res.status).toBe(200);
			});
		return reqG;
	});

});