import axios from "axios";

//const token = (`${window.localStorage.getItem('token')}`);
const token = localStorage.getItem('token');

//cration and authorization with token
export const setPryvUrl = (username = null) => {
	if (username) {
		axios.defaults.baseURL = 'https://' + (`${username}`) + '.pryv.me';
	}
};


export const adminHeader = {
	headers: {
		'Content-Type': 'application/json;charset=UTF-8',
		"Access-Control-Allow-Origin": "*",
		"Authorization": (`${token}`)
	},
	params: {
		limit: 0,
	}
};

export const fetchAllPatientsByStream = {
	headers: {
		'Content-Type': 'application/json;charset=UTF-8',
		"Access-Control-Allow-Origin": "*",
		"Authorization": (`${token}`)
	},
	params: {
		limit: 0,
		streams: ["patients"]
	}
};

export const fetchAllActivitiesByStream = {
	headers: {
		'Content-Type': 'application/json;charset=UTF-8',
		"Access-Control-Allow-Origin": "*",
		"Authorization": (`${token}`)
	},
	params: {
		limit: 0,
		streams: ["activities"]
	}
};

export const parentStream = {
	headers: {
		'Content-Type': 'application/json;charset=UTF-8',
		"Access-Control-Allow-Origin": "*",
		"Authorization": (`${token}`)
	},


	params: {
		limit: 0,
		parentId: "activities"
	}
};